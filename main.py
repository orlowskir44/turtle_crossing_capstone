import random
import time
from turtle import Screen
from player import Player
from car_manager import CarManager
from scoreboard import Scoreboard

screen = Screen()
screen.setup(width=600, height=600)
screen.tracer(0)
screen.listen()

# Turtle:
john = Player()

# Cars:
cars = list()
for x in range(90):
    cars.append(CarManager())

# Scoreboard:
sc = Scoreboard()

# Movement:
screen.onkey(john.move, 'Up')

game_is_on = True

while game_is_on:
    time.sleep(0.08)
    screen.update()
    # Car moves:
    for x in cars:
        z = random.choice(cars)
        z.move()
        # Detect collision with a car [game over]:
        if john.distance(x) <= 15:
            sc.game_over()
            game_is_on = False
    # Lvl up:
    if john.finish_line():
        sc.level()
        john.start()
        for x in cars:
            x.faster()

screen.exitonclick()
