from turtle import Turtle

FONT = ("Courier", 24, "normal")
ALIGN = 'center'
POSITION = ((-200, 260), (-20, 240))


class Scoreboard(Turtle):

    def __init__(self):
        super().__init__()
        self.color('black')
        self.penup()
        self.hideturtle()

        self.lvl = 0
        self.level()

    def level(self):
        # if self.lvl > 1:
        #     self.lvl_up()
        self.clear()
        self.lvl += 1
        self.goto(POSITION[0])
        self.write(f'🐢Level🐢: {self.lvl}', align=ALIGN, font=FONT)
        self.goto(POSITION[1])
        self.write('🏁', font=('', 40, ''))

    def game_over(self):
        self.goto(0, 0)
        self.write(f'🥗Game Over🥗!', align=ALIGN, font=FONT)

    # def lvl_up(self):
    #     self.goto(POSITION[1])
    #     self.write('🥳LEVEL COMPLETED!🥳', align=ALIGN, font=FONT)
