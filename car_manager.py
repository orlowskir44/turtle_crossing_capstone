from turtle import Turtle
import random

COLORS = ["red", "orange", "yellow", "green", "blue", "purple"]
STARTING_MOVE_DISTANCE = 3.3
MOVE_INCREMENT = 0.3


class CarManager(Turtle):

    def __init__(self):
        super().__init__()
        self.shape('square')
        self.color(random.choice(COLORS))
        self.turtlesize(1, 2)
        self.penup()
        self.left(180)
        self.speed_up = 0
        self.random_position_start()

    def random_position_start(self):
        self.goto(random.randint(-200, 1000), random.randint(-210, 210))

    def random_position(self):
        self.goto(random.randint(320, 1000), random.randint(-210, 210))

    def move(self):
        self.forward(STARTING_MOVE_DISTANCE + self.speed_up)
        if self.xcor() < -320:
            self.random_position()

    def faster(self):
        self.speed_up += MOVE_INCREMENT
